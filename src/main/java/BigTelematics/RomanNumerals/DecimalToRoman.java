package BigTelematics.RomanNumerals;

import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class DecimalToRoman implements RomanNumeralGenerator {

	private Map<Integer, String> numerals;

	public DecimalToRoman() {
		numerals = new TreeMap<Integer, String>(Collections.reverseOrder());
		numerals.put(1000, "M");
		numerals.put(900, "CM");
		numerals.put(500, "D");
		numerals.put(400, "CD");
		numerals.put(100, "C");
		numerals.put(90, "XC");
		numerals.put(50, "L");
		numerals.put(40, "XL");
		numerals.put(10, "X");
		numerals.put(9, "IX");
		numerals.put(5, "V");
		numerals.put(4, "IV");
		numerals.put(1, "I");

	}

	@Override
	public String generate(int number) {
		// TODO Auto-generated method stub
		StringBuilder result = new StringBuilder("");

		Iterator setIterator = numerals.entrySet().iterator();
		while (setIterator.hasNext() && number > 0) {
			Map.Entry me = (Map.Entry) setIterator.next();
			while ((Integer) me.getKey() <= number) {
				result.append(me.getValue());
				number -= (Integer) me.getKey();
			}
		}

		return result.toString();
	}

}
