package BigTelematics.RomanNumerals;

public interface RomanNumeralGenerator {
	String generate(int number);
}
