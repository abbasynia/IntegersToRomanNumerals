package BigTelematics.RomanNumerals;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;

public class DecimalToRomanTest {

	static DecimalToRoman gen;

	@BeforeClass
	public static void intialiseDecimalToRomanOnceBeforeClassToShowItIsRerunnable() {
		gen = new DecimalToRoman();
	}

	@Test
	public void testWhenZeroIsPassed() {
		assertEquals("", gen.generate(0));
	}

	@Test
	public void testUseCaseWhenOneIsPassed() {
		assertEquals("I", gen.generate(1));
	}

	@Test
	public void testUseCaseWhenFiveIsPassed() {
		assertEquals("V", gen.generate(5));
	}

	@Test
	public void testUseCaseWhenTwentyIsPassed() {
		assertEquals("XX", gen.generate(20));
	}

	@Test
	public void testUseCaseWhenOneThousandFiveHundredIsPassed() {
		assertEquals("MD", gen.generate(1500));
	}

	@Test
	public void testOddNumberOverHundred() {
		assertEquals("CXXXI", gen.generate(131));
	}

	@Test
	public void testEvenNumberOverHundred() {
		assertEquals("CLXXXVIII", gen.generate(188));
	}

}
