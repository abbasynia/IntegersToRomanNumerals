# IntegerToRomanNumerals

This project is a Java attempt at writing a solution which converts inputs given as Integers, into their Roman Numeral equivalents.

## Running the project
The project was built using maven, so to run it a simple "mvn package" needs to be run in terminal while in the main directory.

## Src
In the src folder, you will find the main solution. 
The solution makes use of a TreeMap, which stores all the neccesary conversions between Integer and Roman Numeral.
When a number in inputted, if it is greater than zero, the TreeMap is iterated from highest key, i.e. 1000 as 'M', all the way to the lowest key, being 1 as 'I'. If the key at hand is greater than the number, it is substracted from the number, appended to the StringBuilder and this carries on until the number is 0. The StringBuilder, which was chosen because of its mutable properties and the fact that a lot of appending could potentially be taking place, is then converted to a String and is returned.

## Test
The testing tests all the given use cases, as well as a few I added in.

## Assumptions

1. The function needs to be rerunnable, so that the object needs to be only initialised once.
2. When a 0 is inputted, the result is just an empty String.
3. The Roman Numeral conversion disregarded the 'Vinculum' system for larger numbers at 5000+. 